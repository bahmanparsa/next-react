export async function postData(url: string, data: object, signal: any): Promise<any> {
  const response = await fetch(url, {
    method: "POST",
    signal: signal,
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  });
  return response.json();
}
