interface TransactionItemProps {
    account_id: string,
    amount: number,
    transaction_id: string,
    created_at?: string,
}
  
export interface Transactions {
    transactions: TransactionItemProps[];
} 
  
export interface Transaction {
    transaction: TransactionItemProps;
} 