import { Fragment } from "react";
import Link from "next/link";
import { Transaction } from "../../types/types";
import TransactionItemRow from "../../components/TransactionItemRow";

const TransactionDetails: React.FC<Transaction> = ({ transaction }) => {
  return (
    <Fragment>
      <div className="container mx-auto px-4 sm:px-8">
        <div className="py-8">
          <div>
            <Link href="/">
              <button className="rounded bg-blue-500 hover:bg-blue-700 py-2 px-4 text-white">
                Back
              </button>
            </Link>
          </div>
          <div>
            <h2 className="text-2xl font-semibold leading-tight text-center">
              Transaction details
            </h2>
          </div>
          <div className="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
            <div className="inline-block min-w-full shadow rounded-lg overflow-hidden">
              <table className="min-w-full leading-normal">
                <tbody>
                  <TransactionItemRow transaction={transaction} />
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export const getStaticPaths = async (): Promise<any> => {
  return {
    paths: [{ params: { id: "05e21d42-14ae-4d8b-bfd9-f16cb0ac68c0" } }],
    fallback: true,
  };
};

export const getStaticProps = async (context: {
  params: { id: string };
}): Promise<any> => {
  try {
    /**
     * To fetch details of a transaction.
     * @param {string} transaction_id (inurl).
     * @returns {object} includes account_id, amount, created_at, transaction_id, .
     */
    const response = await fetch(
      `https://infra.devskills.app/api/accounting/transactions/${context.params.id}`
    );
    const result = await response.json();
    console.log(result);
    return {
      props: {
        transaction: result,
      },
    };
  } catch (error) {
    return { notFound: true };
  }
};

export default TransactionDetails;
