import Head from "next/head";
import TransactionItem from "../components/TransactionItem";
import TransactionForm from "../components/TransactionForm";
import { Transactions } from "../types/types";

const Home: React.FC<Transactions> = ({ transactions }) => {
  return (
    <div>
      <Head>
        <title>Frontend Boilerplate React</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="w-full flex flex-wrap">
        <div className="w-full sm:w-[32%] sm:h-min bg-white p-6 m-[.5%]">
          <TransactionForm />
        </div>
        <div className="w-full sm:w-[66%] m-[.5%] p-6 overflow-y-auto h-[97vh]">
          <h5 className="text-2xl font-semibold leading-tight mb-4 mt-0">
            Transactions history
          </h5>
          <TransactionItem transactions={transactions} />
        </div>
      </div>
    </div>
  );
};

export const getServerSideProps = async (): Promise<any> => {
  try {
    /**
     * To get list of completed transactions.
     * @returns {Array} An array of completed transactions.
     */
    const response = await fetch(
      "https://infra.devskills.app/api/accounting/transactions"
    );
    const result = await response.json();
    console.log(result);
    return {
      props: {
        transactions: result,
      },
    };
  } catch (error) {
    return { notFound: true };
  }
};

export default Home;
