import { Fragment, useState } from "react";
import { postData } from "../utils/postData";
import { useRouter } from "next/router";
import Router from "next/router";
import Spinner from "./Spinner";

const TransactionForm: React.FC = () => {
  const router = useRouter();
  const [state, setState] = useState({
    account_id: "",
    amount: "",
  });
  const [loading, setLoading] = useState(false);

  const handleSubmit: React.FormEventHandler<HTMLFormElement> = (
    event
  ): void => {
    event.preventDefault();
    setLoading(true);
    const controller = new AbortController();
    const signal = controller.signal;
    const abortTimeout = setTimeout(() => {
      controller.abort();
      throw { error: "Submit transaction api aborted" };
    }, 20000);

    /**
     * To submit a transaction.
     * @param {string} account_id Type of the pizza.
     * @param {number} [amount] Amount of the transaction.
     * @returns {object} includes account_id, amount, transaction_id.
     */
    postData(
      "https://infra.devskills.app/api/accounting/transaction",
      state,
      signal
    )
      .then((data) => {
        setLoading(false);
        clearTimeout(abortTimeout);
        console.log(data); // JSON data parsed by `data.json()` call
        setState({
          account_id: "",
          amount: "",
        });

        router.reload();
      })
      .catch((error) => {
        setLoading(false);
        console.warn(error);
      });
  };

  const handleChange: React.ChangeEventHandler<HTMLInputElement> = (
    event
  ): void => {
    const value = event.target.value;
    setState({
      ...state,
      [event.target.name]: value,
    });
  };

  return (
    <Fragment>
      <h5 className="text-2xl font-semibold leading-tight mb-4 mt-0">
        Submit new transaction
      </h5>
      <form
        onSubmit={handleSubmit}
        className="w-full bg-white shadow border p-8"
      >
        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="account_id"
          >
            Account ID
          </label>
          <input
            data-type="account-id"
            className="shadow appearance-none border w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="account_id"
            name="account_id"
            type="text"
            value={state.account_id}
            onChange={handleChange}
            required
          />
        </div>

        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="amount"
          >
            Amount
          </label>
          <input
            data-type="amount"
            className="shadow appearance-none border w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="amount"
            name="amount"
            type="number"
            value={state.amount}
            onChange={handleChange}
            required
          />
        </div>

        <div
          className={`flex relative items-center justify-center ${
            loading ? "pointer-events-none" : "pointer-events-auto"
          }`}
        >
          <input
            data-type="transaction-submit"
            className="w-1/2 bg-blue-500 hover:bg-blue-700 text-white font-bold mt-2 py-2 rounded focus:outline-none focus:shadow-outline cursor-pointer"
            type="submit"
            value={loading ? "" : "Submit"}
          />
          {loading ? <Spinner /> : ""}
        </div>
      </form>
    </Fragment>
  );
};

export default TransactionForm;
