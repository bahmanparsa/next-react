import style from "./Spinner.module.css";

const Spinner: React.FC = () => (
  <div className={(style.spinner, style.spinnerStyle)}></div>
);

export default Spinner;
