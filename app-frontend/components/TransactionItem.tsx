import { Fragment, useMemo } from "react";
import Link from "next/link";
import { Transactions } from "../types/types";
import Image from "next/image";

const TransactionItem: React.FC<Transactions> = ({ transactions }) => {
  let balance = useMemo(() => total(transactions), [transactions]);

  return (
    <Fragment>
      {transactions.map((transaction, i) => (
        <div
          data-type="transaction"
          data-account-id={transaction.account_id}
          data-amount={transaction.amount}
          data-balance={balance}
          className="w-full bg-white shadow border py-3 px-5 my-4"
          key={transaction.transaction_id}
        >
          {`Transfered $${transaction.amount} from account ${transaction.account_id}`}
          {i === 0 && <br />}
          {i === 0 &&
            `The current account balance ${balance < 0 ? "-" : ""}$${Math.abs(
              balance
            )}`}
          {
            <Link href={`/transaction-details/${transaction.transaction_id}`}>
              <div className="inline mx-2 px-2.5 py-0.5 bg-cyan-400 rounded-full text-slate-500 font-semibold hover:bg-blue-700 hover:text-white cursor-pointer">
                i
              </div>
            </Link>
          }
        </div>
      ))}
    </Fragment>
  );
};

function total(data) {
  const items = data.filter(
    (item) => item.account_id.indexOf(data[0].account_id) !== -1
  );
  console.log(items);
  let array = items.map((a) => a.amount);
  return array.reduce((a, b) => a + b);
}

export default TransactionItem;
