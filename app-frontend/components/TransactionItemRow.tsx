import { Fragment } from "react";
import { Transaction } from "../types/types";

const TransactionItemRow: React.FC<Transaction> = ({ transaction }) => {
  return (
    <Fragment>
      {transaction &&
        Object.keys(transaction).map((key) => {
          return (
            <tr key={key}>
              <td className="w-1/3 px-5 py-5 border-b border-gray-200 bg-white text-sm">
                <div className="flex">
                  <div className="ml-3">
                    <p className="text-gray-900 whitespace-no-wrap">{key}</p>
                  </div>
                </div>
              </td>
              <td className="w-2/3 px-5 py-5 border-b border-gray-200 bg-white text-sm">
                <p className="text-gray-900 whitespace-no-wrap">
                  {transaction[key]}
                </p>
              </td>
            </tr>
          );
        })}
    </Fragment>
  );
};

export default TransactionItemRow;
